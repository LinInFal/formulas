﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace modul_ucheb_praktika
{
    /// <summary>
    /// Логика взаимодействия для Stepen2formula.xaml
    /// </summary>
    public partial class Stepen2formula : Window
    {
        public Stepen2formula()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a = Convert.ToDouble(txtboxA.Text);
            double b = Convert.ToDouble(txtboxB.Text);
            double c = Convert.ToDouble(txtboxC.Text);

            double res = Convert.ToDouble(Math.Pow(a, 2) - ((2 * Math.Pow(a, c) + 5 * Math.Pow(b, c)) / Math.Pow(c, b)));
            txtblock1.Text = res.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Stepen stepen = new Stepen();
            stepen.Show();
            Hide();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
