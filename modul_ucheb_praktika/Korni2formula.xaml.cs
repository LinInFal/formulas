﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace modul_ucheb_praktika
{
    /// <summary>
    /// Логика взаимодействия для Korni2formula.xaml
    /// </summary>
    public partial class Korni2formula : Window
    {
        public Korni2formula()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double a = Convert.ToDouble(txtboxA.Text);
            double b = Convert.ToDouble(txtboxB.Text);
            double c = Convert.ToDouble(txtboxC.Text);

            double res = Convert.ToDouble((Math.Sqrt(a) / Math.Sqrt(b - 1)) - 3 * Math.Sqrt(c));
            txtblock1.Text = res.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Korni korni = new Korni();
            korni.Show();
            Hide();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
